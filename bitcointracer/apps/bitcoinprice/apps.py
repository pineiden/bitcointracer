from django.apps import AppConfig


class BitcoinpriceConfig(AppConfig):
    name = 'bitcoinprice'
