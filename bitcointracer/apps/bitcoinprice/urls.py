
from django.conf.urls import url
from django.urls import path
# from django.contrib import admin
from apps.bitcoinprice import views

app_name='bitcoinprice'

urlpatterns = [
    path('list/', views.BitcoinPriceList.as_view(), name='bitcoinprice-list'),
    path('view/<int:id>/', views.BitcoinPriceDetail.as_view(), name="bitcoinprice-detail"),
    path('create/', views.BitcoinPriceCreate.as_view(), name="bitcoinprice-create"),
    path('edit/<int:id>/', views.BitcoinPriceUpdate.as_view(), name="bitcoinprice-update"),
    path('delete/<int:id>/', views.BitcoinPriceDelete.as_view(), name="bitcoinprice-delete"),
    ]