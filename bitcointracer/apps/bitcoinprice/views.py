from django.shortcuts import render
from django.urls import reverse_lazy
from .models import BitcoinPrice
# Create your views here.
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView

class BitcoinPriceDetail(DetailView):
	model=BitcoinPrice
	pk_url_kwarg = 'id'

class BitcoinPriceList(ListView):
	model=BitcoinPrice
	allow_empty = True 
	ordering = 'date' 
	paginate_by = 50 

class BitcoinPriceCreate(CreateView):
	model=BitcoinPrice
	fields=['date','value']
	pk_url_kwarg = 'id'

class BitcoinPriceUpdate(UpdateView):
	model=BitcoinPrice
	pk_url_kwarg = 'id'
	fields=['date','value']

class BitcoinPriceDelete(DeleteView):
	model=BitcoinPrice
	pk_url_kwarg = 'id'
	success_url=reverse_lazy("home:index")
