from django.contrib import admin

# Register your models here.
from .models import BitcoinPrice

class BitcoinPriceAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(BitcoinPrice, BitcoinPriceAdmin)