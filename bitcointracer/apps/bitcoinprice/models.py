from django.db import models
from datetime import datetime

from django.urls import reverse

# Create your models here.
class BitcoinPrice(models.Model):
	created=models.DateTimeField(auto_now_add=True, null=False)
	date=models.DateField(default=datetime.today, null=False, unique=True)
	value=models.FloatField(null=False, blank=True ,default=None)

	def __str__(self):
		return "%s -> %s" % (self.date, self.value)

	@property
	def BitcoinPrice(self):
		return self.__str__()

	def get_absolute_url(self):
		return reverse('bitcoinprice:bitcoinprice_detail', kwargs={'pk':self.pk})

	class Meta:
		app_label='bitcoinprice'
		verbose_name='Bitcoin Price'
		verbose_name_plural='Bitcoin Prices'
		ordering=('date','value')       
