
from django.conf.urls import url
from django.urls import path
# from django.contrib import admin
from apps.home import views

app_name='home'

urlpatterns = [
    path('', views.HomeView.as_view(), name='index'),
    path('chart/', views.ChartView.as_view(), name="chart"),
    ]