from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView
from apps.bitcoinprice.models import BitcoinPrice
from django.core import serializers

class HomeView(TemplateView):
	template_name = "home/index.html"

class ChartView(TemplateView):
	template_name = "home/chart.html" 

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['bcps'] = BitcoinPrice.objects.all()
		return context
