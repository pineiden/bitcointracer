
$.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function (data) {
    // Create the chart

    var hiJson = prices.map(function(d) {
      return [d.date.getTime(), d.value]
    });

    Highcharts.stockChart('container', {


        rangeSelector: {
            selected: 1
        },

        title: {
            text: 'Historical Bitcoin Price'
        },

        series: [{
            name: 'BitcoinPrice',
            data: hiJson,
            tooltip: {
                valueDecimals: 2
            }
        }]
    });
});
