

import os, sys, django

BASE_DIR = os.path.dirname(
	os.path.dirname(
		os.path.dirname(
			os.path.abspath(__file__)
			)
		)
	)

sys.path.append(BASE_DIR)
sys.path.append(BASE_DIR +'/bitcointracer')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bitcointracer.settings")


import django
django.setup()


from datetime import datetime
from exchanges.coindesk import CoinDesk
from apps.bitcoinprice.models import BitcoinPrice

from datetime import timedelta

def load_bitcoins(json):
	for key,value in b.items():
		date=datetime.strptime(key, "%Y-%m-%d")
		print("%s,%s"%(date, value))
		bcp=BitcoinPrice(date=date, value=value)
		try:
			bcp.save()
		except Exception as e:
			print("Error when save new instance %s" %e)
			raise e


if __name__ == "__main__":
	b=None
	blast=BitcoinPrice.objects.all().order_by('date').last()
	last_date=blast.date
	new_date=last_date+timedelta(days=1)
	print("Dates before: %s and after: %s"%(last_date, new_date))
	tosearch=new_date.isoformat()
	print("To search: %s"%tosearch)
	try:
		b=CoinDesk().get_historical_data_as_dict(start=tosearch, end=None)
		print(b)
		print(len(b.keys()))
		load_bitcoins(b)
	except Exception as e:
		raise e

