

import os, sys, django

BASE_DIR = os.path.dirname(
	os.path.dirname(
		os.path.dirname(
			os.path.abspath(__file__)
			)
		)
	)

sys.path.append(BASE_DIR)
sys.path.append(BASE_DIR +'/bitcointracer')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bitcointracer.settings")
print(os.environ["DJANGO_SETTINGS_MODULE"])
print(sys.path)

print("Base DIR: %s" %BASE_DIR)

import django
django.setup()


from datetime import datetime
from exchanges.coindesk import CoinDesk
from apps.bitcoinprice.models import BitcoinPrice

from datetime import datetime, timedelta
from datetime import timezone
import pytz


def load_bitcoins(json):
	for key,value in b.items():
		date=datetime.strptime(key, "%Y-%m-%d").date()
		print("%s,%s"%(date, value))
		bcp=BitcoinPrice(date=date, value=value)
		bcp.save()


if __name__ == "__main__":
	b=None
	try:
		b=CoinDesk().get_historical_data_as_dict(start='2013-09-01', end=None)
		print(b)
		print(len(b.keys()))
	except Exception as e:
		raise e

	load_bitcoins(b)
